<?php
return array(
    ////contact page    
    'Name' => 'Имя',
    'Email' => 'Мейл',
    'Subject' => 'Заголовок',
    'Body' => 'Текст',
    'verifyCode' => 'Код верификации',
    'Submit' => 'Отправить',
    
    ////main menu 
    'Home' => 'Главная',
    'Clients' => 'Клиенты',
    'Invoice' => 'Счет-фактура',
    'Companies' => 'Компании',
    'Services' =>   'Услуги',
    'Taxes' => 'Налоги',
    'Credits' => 'Кредиты',
    'Settings' => 'Настройки',
    'About' => 'О нас',
    'Contact' => 'Контакты',
    'Login' => 'Логин',
    'Logout' => 'Выход',
    
    //// Login page
    'LoginHeaderText' => 'Вход в систему',
    'LoginBodyText' => 'Please fill out the following form with your login credentials:',
    
    //// Main page
    'Welcome' => 'Добро Пожаловать',
    'main_page_text' => 'Ваш ID - ',
    
    //// Clients page
    'ClientsHeaderText' => 'Клиенты',
    'ClientViewText' => 'Просмотр клиента',
    'ClientCreateText' => 'Создать клиент',
    
    //// Invoice page
    'InvoiceHeaderText' => 'Счет-фактуры',
    'InvoiceHeaderCreateText' => 'Создать Счет-фактуру',
    'InvoiceClientNameText' => 'Клиент',
    'InvoiceNameText' => 'Название',
    'InvoiceServiceText' => 'Услуга',
    'InvoiceDateText' => 'Дата',
    'InvoiceCompanyText' => 'Компания',
    'InvoicePriceText' => 'Сумма',
    'InvoiceCurrencyText' => 'Валюта',
    'InvoiceCountText' => 'Количество',
    'InvoiceVATText' => 'НДС',
    'InvoiceDiscountText' => 'Скидка',
    'InvoicePriceCalcText' => 'Подсчет цены',
    'InvoiceSaveText' => 'Сохранить',
    'InvoiceToPayText' => 'Сумма к оплате',
    'InvoicePayWithText' => 'Оплатить с помощью',
    'InvoicePayText' => 'Оплатить',
    
    //// Service page
    'ServiceHeaderText' => 'Услуги',
    
    //// Tax page
    'TaxHeaderText' => 'Налоги',
    'TaxVATSettingText' => 'Настройки НДС',
    'TaxSettingText' => 'Настройки налогов',
    
    //// Setting page
    'SettingHeaderText' => 'Настройки',
);