<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
	return array(
	    // captcha action renders the CAPTCHA image displayed on the contact page
	    'captcha' => array(
		'class' => 'CCaptchaAction',
		'backColor' => 0xFFFFFF,
	    ),
	    // page action renders "static" pages stored under 'protected/views/site/pages'
	    // They can be accessed via: index.php?r=site/page&view=FileName
	    'page' => array(
		'class' => 'CViewAction',
	    ),
	);
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
	// renders the view file 'protected/views/site/index.php'
	// using the default layout 'protected/views/layouts/main.php'
	$this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
	if ($error = Yii::app()->errorHandler->error) {
	    if (Yii::app()->request->isAjaxRequest)
		echo $error['message'];
	    else
		$this->render('error', $error);
	}
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
	$model = new ContactForm;
	if (isset($_POST['ContactForm'])) {
	    $model->attributes = $_POST['ContactForm'];
	    if ($model->validate()) {
		$name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
		$subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
		$headers = "From: $name <{$model->email}>\r\n" .
			"Reply-To: {$model->email}\r\n" .
			"MIME-Version: 1.0\r\n" .
			"Content-Type: text/plain; charset=UTF-8";

		mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
		Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
		$this->refresh();
	    }
	}
	$this->render('contact', array('model' => $model));
    }

    /**
     * Displays the register page
     */
    public function actionRegister() {
	$model = new RegisterForm;
	$newUser = new User;
	$newSetting = new Setting;

	// if it is ajax validation request
	if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
	    echo CActiveForm::validate($model);
	    Yii::app()->end();
	}

	// collect user input data
	if (isset($_POST['RegisterForm'])) {

	    $newUser->attributes = $_POST['RegisterForm'];	    
	    $newUser->setAttributes(array(
		'register_date' => date('Y-m-d H:i:s'),
		'last_login' => date('Y-m-d H:i:s'),	
	    ));
	    
	    $newUser->user_id = uniqid();
	    $rawPass = $newUser->password;
	    $newUser->password = md5($newUser->password);
	    
	     //echo '<pre>';
	    //print_r($newUser->attributes); exit;
	
	    if ($newUser->validate()) {
		if ($newUser->save()) {
		    if (empty($newUser->parent_id)) {
			$sql = "UPDATE user SET parent_id = LAST_INSERT_ID() WHERE id = LAST_INSERT_ID()";
			Yii::app()->db->createCommand($sql)->execute();			
		    }
		    $newSetting->user_id = $newUser->id;
		    $newSetting->save();
		    
		    $body = 'Your login - '.$newUser->user_id."\r\n";
		    $body .= 'Your password - '.$rawPass."\r\n";
		    
		    $name = '=?UTF-8?B?' . base64_encode('Logistic Biling System') . '?=';
		    $subject = '=?UTF-8?B?' . base64_encode('Registration information') . '?=';
		    $headers = "From: $name <".Yii::app()->params['adminEmail'].">\r\n" .
			    "Reply-To: ".Yii::app()->params['adminEmail']."\r\n" .
			    "MIME-Version: 1.0\r\n" .
			    "Content-Type: text/plain; charset=UTF-8";

		    if (mail($newUser->mail, $subject, $body, $headers)) {
			Yii::app()->user->setFlash('success', 'Registration Successful. Registration information send to your mail.');
			//$this->refresh();
			//$this->redirect('/index.php');
			$this->redirect(Yii::app()->user->returnUrl);
			//$this->redirect('index.php/site/index');
		    }

		    /*
		    
		    //print_r($rawPass); exit;
		    $identity = new UserIdentity($newUser->user_id, $rawPass);
		    $identity->authenticate();
		    
		    
		    if ($identity->errorCode === UserIdentity::ERROR_NONE) {
			//$duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
			$duration = 3600 * 24 * 30; // 30 days
			Yii::app()->user->login($identity, $duration);
			$newUser->unsetAttributes();
			$newSetting->unsetAttributes();
			//$this->redirect(array('site/index', 'id' => $newUser->id));
			$this->redirect('/index.php/site/index');			
		    }
		    */
		}
	    }	   
	}
	// display the register form
	$this->render('register', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
	$model = new LoginForm;

	// if it is ajax validation request
	if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
	    echo CActiveForm::validate($model);
	    Yii::app()->end();
	}

	// collect user input data
	if (isset($_POST['LoginForm'])) {
	    $model->attributes = $_POST['LoginForm'];
	    // validate user input and redirect to the previous page if valid
	    if ($model->validate() && $model->login())
		$this->redirect('index.php/site/index');
	    //$this->redirect(Yii::app()->user->returnUrl);
	}
	// display the login form
	$this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
	Yii::app()->user->logout();
	$this->redirect(Yii::app()->homeUrl);
    }

}
