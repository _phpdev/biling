<?php

return array(
    '6' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Guest',
        'bizRule' => null,
        'data' => null
    ),
    '5' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Client',
        'children' => array(
            '6', // унаследуемся от гостя
        ),
        'bizRule' => null,
        'data' => null
    ),
    '4' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'User',
        'children' => array(
            '5', // унаследуемся от клиента
        ),
        'bizRule' => null,
        'data' => null
    ),
    '3' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Manager',
        'children' => array(
            '4',          // позволим менеджеру всё, что позволено пользователю
        ),
        'bizRule' => null,
        'data' => null
    ),
    '2' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Administrator',
        'children' => array(
            '3',         // позволим админу всё, что позволено менеджеру
        ),
        'bizRule' => null,
        'data' => null
    ),
    '1' => array(
        'type' => CAuthItem::TYPE_ROLE,
        'description' => 'Root',
        'children' => array(
            '2',         // позволим root всё, что позволено админу
        ),
        'bizRule' => null,
        'data' => null
    ),
);