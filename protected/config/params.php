<?php

return array(
	'adminEmail' => 'webmaster@example.com',
	'languages' => array('ru' => 'Русский', 'de' => 'Deutsch', 'en' => 'English'),
	'currency' => array('1' => 'EUR','2' => 'USD','3' => 'GBP', '4' => 'RUR'),
	'imagePath' => '/images/',
    );
