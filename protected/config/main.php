<?php

// uncomment the following to define a path alias
//Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Biling',
    'defaultController' => 'site/login',
    'sourceLanguage' => '00',
    'language' => 'en',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
	'application.models.*',
	'application.components.*',
	'application.helpers.*',
    ),
    'modules' => array(
	// uncomment the following to enable the Gii tool

	'gii' => array(
	    'class' => 'system.gii.GiiModule',
	    'password' => '123',
	    // If removed, Gii defaults to localhost only. Edit carefully to taste.
	    'ipFilters' => array('127.0.0.1', '::1'),
	),
    /**/
    ),
    // application components
    'components' => array(
	'user' => array(
	    'class' => 'WebUser',
	    // enable cookie-based authentication
	    'allowAutoLogin' => true,
	),
	'image' => array(
	    'class' => 'application.extensions.image.CImageComponent',
	    // GD or ImageMagick
	    'driver' => 'GD',
	// ImageMagick setup path
	//'params'=>array('directory'=>'/opt/local/bin'),
	),
	'authManager' => array(
	    // Будем использовать свой менеджер авторизации
	    'class' => 'PhpAuthManager',
	    // Роль по умолчанию. Все, кто не админы, модераторы и юзеры — гости.
	    'defaultRoles' => array('guest'),
	),
	// uncomment the following to enable URLs in path-format
	'urlManager' => array(
	    'class' => 'application.components.UrlManager',
	    'urlFormat' => 'path',
	    'showScriptName' => true,
	    'rules' => array(
		'<language:(ru|de|en)>/' => 'site/index',
		///@todo change invoice urls
		'<language:(ru|de|en)>/<controller:invoice>/<id:MM\d+>' => 'invoice/view',
		'<language:(ru|de|en)>/<controller:invoice>/<action:\w+>/<id:MM\d+>' => 'invoice/<action>',
		'<language:(ru|de|en)>/<action:(contact|login|logout)>/*' => 'site/<action>',
		'<language:(ru|de|en)>/<controller:\w+>/<id:\d+>' => '<controller>/view',
		'<language:(ru|de|en)>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
		'<language:(ru|de|en)>/<controller:\w+>/<action:\w+>/*' => '<controller>/<action>',
	    ),
	),
	'Paypal' => array(
			'class'=>'application.components.Paypal',
			'apiUsername' => 'YOUR_API_USERNAME',
			'apiPassword' => 'YOUR_API_PASSWORD',
			'apiSignature' => 'YOUR_API_SIGNATURE',
			'apiLive' => false,
			
			'returnUrl' => 'paypal/confirm/', //regardless of url management component
			'cancelUrl' => 'paypal/cancel/', //regardless of url management component
		),
	'request' => array(
	    'enableCookieValidation' => true,
	    'enableCsrfValidation' => true,
	),
	// uncomment the following to use a MySQL database
	'db' => array(
	    'connectionString' => 'mysql:host=localhost;dbname=logistic_biling',
	    'emulatePrepare' => true,
	    'username' => 'logistic_biling',
	    'password' => '9k@#u32n@m1ce2',
	    'charset' => 'utf8',
	),
	/**/
	'errorHandler' => array(
	    // use 'site/error' action to display errors
	    'errorAction' => 'site/error',
	),
	'log' => array(
	    'class' => 'CLogRouter',
	    'routes' => array(
		array(
		    'class' => 'CFileLogRoute',
		    'levels' => 'error, warning',
		),
	    // uncomment the following to show log messages on web pages
	    /*
	      array(
	      'class'=>'CWebLogRoute',
	      ),
	     */
	    ),
	),	
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>require('params.php'),
    
);
