<?php

class m141104_154255_create_table_map_invoice_service extends CDbMigration
{
	public function up()
	{
	    $this->createTable('map_invoice_service', array(
			'id'             => 'pk',
			'invoice_id' => "INT(11) NULL DEFAULT NULL",
			'service_id' => "INT(11) NULL DEFAULT NULL"));
	}

	public function down()
	{
		echo "m141104_154255_create_table_map_invoice_service does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}