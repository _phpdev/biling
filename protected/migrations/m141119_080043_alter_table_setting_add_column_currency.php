<?php

class m141119_080043_alter_table_setting_add_column_currency extends CDbMigration {

    public function up() {
	$this->addColumn('setting', 'def_currency', 'VARCHAR(10)');
    }

    public function down() {
	echo "m141119_080043_alter_table_setting_add_column_currency does not support migration down.\n";
	return false;
    }

}
