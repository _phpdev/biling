<?php

class m141104_160821_alter_table_Invoice extends CDbMigration
{
	public function up()
	{
	    $this->alterColumn('invoice', 'service', 'VARCHAR(255)');
	}

	public function down()
	{
		echo "m141104_160821_alter_table_Invoice does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}