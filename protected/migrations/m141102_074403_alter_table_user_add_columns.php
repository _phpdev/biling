<?php

class m141102_074403_alter_table_user_add_columns extends CDbMigration {

    public function up() {
	
	$this->addColumn('user', 'country', 'VARCHAR(255)');
	$this->addColumn('user', 'city', 'VARCHAR(255)');
	$this->addColumn('user', 'street', 'VARCHAR(255)');
	$this->addColumn('user', 'post_index', 'int(11)');
	$this->addColumn('user', 'phone', 'VARCHAR(255)');
	$this->addColumn('user', 'web_site', 'VARCHAR(255)');
	$this->addColumn('user', 'mail', 'VARCHAR(255)');
	$this->addColumn('user', 'vat_number', 'VARCHAR(255)');
	$this->addColumn('user', 'activity', 'VARCHAR(255)');
	$this->addColumn('user', 'resp_person', 'VARCHAR(255)');	
    }

    public function down() {
	echo "m141102_074403_alter_table_user_add_columns does not support migration down.\n";
	return false;
    }

    /*
      // Use safeUp/safeDown to do migration with transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
