<?php

class m141104_074059_alter_table_Company_add_columns extends CDbMigration {

    public function up() {

	$this->addColumn('company', 'country', 'VARCHAR(255)');
	$this->addColumn('company', 'city', 'VARCHAR(255)');
	$this->addColumn('company', 'street', 'VARCHAR(255)');
	$this->addColumn('company', 'post_index', 'int(11)');
	$this->addColumn('company', 'phone', 'VARCHAR(255)');
	$this->addColumn('company', 'web_site', 'VARCHAR(255)');
	$this->addColumn('company', 'mail', 'VARCHAR(255)');
	$this->addColumn('company', 'vat_number', 'VARCHAR(255)');
	$this->addColumn('company', 'activity', 'VARCHAR(255)');
	$this->addColumn('company', 'resp_person', 'VARCHAR(255)');
    }

    public function down() {
	echo "m141102_074403_alter_table_user_add_columns does not support migration down.\n";
	return false;
    }

}
