<?php

class m141104_085418_alter_table_Invoice_add_columns extends CDbMigration {

    public function up() {
	$this->addColumn('invoice', 'service', 'int(11)');
	$this->addColumn('invoice', 'count', 'int(11)');
	$this->addColumn('invoice', 'vat', 'int(3)');
	$this->addColumn('invoice', 'discount', 'int(3)');
    }

    public function down() {
	echo "m141104_085418_alter_table_Invoice_add_columns does not support migration down.\n";
	return false;
    }

    /*
      // Use safeUp/safeDown to do migration with transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
