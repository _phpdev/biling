<?php

/**
 * Description of WebUser
 *
 * @author ars
 */
class WebUser extends CWebUser {

    /**
     * Overrides a Yii method that is used for roles in controllers (accessRules).
     *
     * @param string $operation Name of the operation required (here, a role).
     * @param mixed $params (opt) Parameters for this operation, usually the object to access.
     * @return bool Permission granted?
     */
    private $_model = null;

    public function getRole() {
	if ($user = $this->getModel()) {
	    // в таблице User есть поле role
	    return $user->role;
	}
    }

    private function getModel() {
	if (!$this->isGuest && $this->_model === null) {
	    $this->_model = User::model()->findByPk($this->id, array('select' => 'role'));
	}
	return $this->_model;
    }

}
