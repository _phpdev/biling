<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
	//echo $this->username;
	$user = User::model()->findByAttributes(array('user_id' => $this->username));
	//print_r($user); exit;
	//echo $this->username.' | '.$user->password.'<br>'; exit;
	//echo md5($this->password). ' | '. $user->password;
	if ($user === null) { // No user found!
	    $this->errorCode = self::ERROR_USERNAME_INVALID;
	} else if ($user->password !== md5($this->password)) { // Invalid password!
	    $this->errorCode = self::ERROR_PASSWORD_INVALID;
	} else { // Okay!
	    $this->_id = $user->id;
	    //$this->user_name = $user->name;
	    //echo $user->name; exit;
	   // $this->setState('user_name', $user->last_login);
	    //$this->setState('roles', $user->role);
	    $this->errorCode = self::ERROR_NONE;
	}
	return !$this->errorCode;

	/*
	  $users = array(
	  // username => password
	  'admin' => 'admin',
	  );
	  if (!isset($users[$this->username]))
	  $this->errorCode = self::ERROR_USERNAME_INVALID;
	  elseif ($users[$this->username] !== $this->password)
	  $this->errorCode = self::ERROR_PASSWORD_INVALID;
	  else
	  $this->errorCode = self::ERROR_NONE;
	  return !$this->errorCode;
	 * 
	 */
    }

    public function getId() {
	return $this->_id;
    }

}
